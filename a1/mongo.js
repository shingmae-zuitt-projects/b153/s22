/* DATA MODEL DESIGN

User document:
{
    _id : <ObjectId1>,
    user : "Shing"
}

{
    _id : <ObjectId2>
    user : "Mae"
}

Product documents:
{
    _id : <ObjectId3>,
    productName : "Attack on Titan manga",
    category : "books",
    price : 500,
    isAvailable: true

}

{
    _id : <ObjectId4>,
    productName : "Ichiran Ramen",
    category : "food",
    price : 250,
    isAvailable : true 

}

{
    _id : <ObjectId5>,
    productName : "Hello Kitty Notebook",
    category : "school supplies",
    price : 50,
    isAvailable : true 
}

Order document:
{
    _id: <ObjectId6>,
    user: <ObjectId1>,
    products : [
        {
            productId : <ObjectId3>
            qty : 10
        }
    ],
    totalPrice : 2500,
    purchasedOn : 02-03-2022
}

{
    _id: <ObjectId7>,
    user: <ObjectId2>,
    products : [
        {
            productId : <ObjectId1>
            qty : 1
        },
        {
            productId : <ObjectId2>
            qty : 3
        },
        {
            productId : <ObjectId3>
            qty : 1
        }
    ],
    totalPrice : 1300,
    purchasedOn : 02-02-2022
}

*/